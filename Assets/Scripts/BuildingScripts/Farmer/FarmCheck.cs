﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmCheck : MonoBehaviour
{

    public GameManager gameManager;

    public LayerMask Farm;
    private static bool farming = false;

    public void Start()
    {

    }
    public void CheckForFarms()
    {
        if (farming)
        {
            return;
        }

        gameManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 250, Farm);

        if (hitColliders.Length >= 1)
        {
            gameManager.StartFarming();
            farming = true;
        }
    }
}
