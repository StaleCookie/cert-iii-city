﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{
    public GameObject PauseMenu;


    public void Start()
    {
        if(PauseMenu != null)
        PauseMenu.SetActive(false);
    }
    public void Play()
    {
        SceneManager.LoadScene("MainLevel");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OnPaused()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0;
    }


    public void Resume()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);

    }

    private void Update()
    {
        if(Input.GetButtonDown("Cancel"))
        {
            OnPaused();
        }
    }
}
