﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodCheck : MonoBehaviour
{

    public GameManager gameManager;

    public LayerMask Tree;

    private static bool chopping = false;
    public void CheckForTrees()
    {
        Debug.Log("Test");
        if (chopping)
        {
            return;
        }

        gameManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 250, Tree);

        if (hitColliders.Length >= 1)
        {
            Debug.Log("Test2");
            gameManager.StartChopping();
            chopping = true;
        }
    }
}
