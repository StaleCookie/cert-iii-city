﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class GameManager : MonoBehaviour
{
    public List<GameObject> FarmerHouses = new List<GameObject>();
    public List<GameObject> AI = new List<GameObject>();

    public List<GameObject> LumberMills = new List<GameObject>();
    public Vector3 aiSpawnLocation;
    private int FarmHouseInt;

    private GameObject CurrentHouse;
    public Vector3 defaultSpawnLocation;
    private Vector3 oldSpawnLocation;

    private Vector3 newSpawnLocation;

    public gamecontroller GameController;

    public float Food;

    public float Wood = 30;

    public WoodCheck woodCheck;

    private GameObject CurrentMill;

    private int LumberMillInt;

    // Start is called before the first frame update

    public void Awake()
    {
        
    }
    void Start()
    {
        AssignFarmerHouse();

        AssignLumberMill();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignFarmerHouse()
    {
        FarmerHouses.Clear();
        FarmerHouses.AddRange(GameObject.FindGameObjectsWithTag("Farmer House"));
        FarmHouseInt = 0;
        if (FarmerHouses.Count >= 1)
        {
            foreach(GameObject item in FarmerHouses)
            {
                CurrentHouse = FarmerHouses.ElementAt(FarmHouseInt);
                CurrentHouse.GetComponent<FarmerHouseVariables>().HouseNum = FarmHouseInt;
                CurrentHouse.GetComponent<FarmCheck>().CheckForFarms();

                FarmHouseInt++;
            }
        }
        
    }

    public void AssignLumberMill()
    {
        LumberMills.Clear();
        LumberMills.AddRange(GameObject.FindGameObjectsWithTag("Lumber Mill"));
        LumberMillInt = 0;
        if (LumberMills.Count >= 1)
        {
            foreach (GameObject item in LumberMills)
            {
                CurrentMill = LumberMills.ElementAt(LumberMillInt);
                CurrentMill.GetComponent<LumberMillVariables>().LumberMillNum = LumberMillInt;
                CurrentMill.GetComponent<WoodCheck>().CheckForTrees();

                LumberMillInt++;
            }
        }

    }


    public void StartFarming()
    {
        Debug.Log("Farm Starting");

        InvokeRepeating("FarmTimer", 0, 1);
    }


    public void FarmTimer()
    {
        Food += FarmHouseInt;
        Debug.Log(Food);
    }




    public void StartChopping()
    {
        Debug.Log("Chopping");
        InvokeRepeating("WoodTimer", 0, 4);
    }


    public void WoodTimer()
    {
        Wood += LumberMillInt;
        Debug.Log(Wood);
    }

}
