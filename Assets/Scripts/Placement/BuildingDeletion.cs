﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDeletion : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (!hit.transform.CompareTag("Terrain") && !hit.transform.CompareTag("people"))
                {
                    Destroy(hit.transform.root.gameObject);
                }
            }
        }

    }
}
