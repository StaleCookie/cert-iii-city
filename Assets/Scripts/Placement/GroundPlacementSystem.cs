﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPlacementSystem : MonoBehaviour
{
    public HouseValues[] buildings;
    public GameObject target;
    public GameObject structure;
    public GameObject Roadspot;
    public GameObject buildingmenu;

    RectTransform m_RectTransform;

    private bool secondClick;

    Vector3 Roadstart;
    Vector3 Roadend;
    Vector3 truePos;
    public float gridSize;

    private List<GameObject> placeableObjectPrefab;

    public bool InUI;

    public List<Transform> BuildingsBuildings;

    private Vector3 buildingPos;
    private GameObject currentBuilding;
    bool placingRoad;

    private Vector3 scaleChange;

    private GameObject gameManagerObject;
    private GameManager gameManager;

    private GameObject buildingToBePlaced;

    [System.Serializable]
    public class HouseValues
    {
        public GameObject building;
        public float wood;
    }

    public void Start()
    {
        secondClick = false;
        GameObject gameControllerObject = GameObject.FindWithTag("Game Controller");
        gamecontroller gameControllerScript = gameControllerObject.GetComponent<gamecontroller>();

    }

    public void Update()
    {
        if (!InUI && currentBuilding != null)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (Input.GetMouseButtonDown(0) && !placingRoad)
                {
                    SetColliders(currentBuilding, true);
                    currentBuilding = null;


                    if (buildingToBePlaced != null && buildingToBePlaced.name == "FarmerHouse")
                    {
                        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

                        Debug.Log("Assign House Working");
                        gameManager.AssignFarmerHouse();
                        buildingToBePlaced = null;
                    }


                    if (buildingToBePlaced != null && buildingToBePlaced.name == "Farm")
                    {
                        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

                        Debug.Log("Assign House Working");
                        gameManager.AssignFarmerHouse();
                        buildingToBePlaced = null;
                    }

                    if (buildingToBePlaced != null && buildingToBePlaced.name == "LumberMill")
                    {
                        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

                        Debug.Log("LumberMill Working");
                        gameManager.AssignLumberMill();
                        buildingToBePlaced = null;
                    }
                }

                if (hit.transform.CompareTag("Terrain"))
                {
                    buildingPos = hit.point;
                }



                if (placingRoad)
                {
                    NewRoad(hit.point);
                }
            }
        }

        if (currentBuilding != null)
        {
            target.transform.position = buildingPos;

            target.transform.position += new Vector3(0, 0.1f, 0);
        }


    }

    public void NewBuilding(GameObject buildingPrefab)
    {
        GameObject gameControllerObject = GameObject.FindWithTag("Game Manager");
        GameManager gameControllerScript = gameControllerObject.GetComponent<GameManager>();

        HouseValues building = null;
        foreach (HouseValues hv in buildings)
        {
            if (hv.building == buildingPrefab)
            {
                building = hv;
            }
        }

        if (gameControllerScript.Wood >= building.wood)
        {
            gameControllerScript.Wood -= building.wood;
            currentBuilding = Instantiate(buildingPrefab, buildingPos, Quaternion.identity) as GameObject;
            SetColliders(currentBuilding, false);
            structure = currentBuilding;
            buildingmenu.SetActive(false);

            buildingToBePlaced = buildingPrefab;
        }


    }
    public void NewRoad(Vector3 mousePosition)
    {
        if (!Input.GetMouseButtonDown(0))
            return;
        if (!secondClick)
        {
            truePos.x = Mathf.Floor(mousePosition.x / gridSize) * gridSize;
            truePos.y = mousePosition.y;//Mathf.Floor(target.transform.position.y / gridSize) * gridSize;
            truePos.z = Mathf.Floor(mousePosition.z / gridSize) * gridSize;

            Roadstart = truePos;
            secondClick = true;
        }
        else
        {

            truePos.x = Mathf.Floor(mousePosition.x / gridSize) * gridSize;
            truePos.y = mousePosition.y;//Mathf.Floor(target.transform.position.y / gridSize) * gridSize;
            truePos.z = Mathf.Floor(mousePosition.z / gridSize) * gridSize;

            Roadend = truePos;
            secondClick = false;
            float distance = Vector3.Distance(Roadstart, Roadend);
            Vector3 midpoint = Vector3.Lerp(Roadstart, Roadend, 0.5f);
            Vector3 direction = Roadstart - Roadend;
            //Roadspot = Instantiate(roadPrefab, midpoint, Quaternion.identity) as GameObject;
            Roadspot = currentBuilding;
            Roadspot.transform.position = midpoint;
            Roadspot.transform.localScale = new Vector3(distance, 0.2f, 1);
            Roadspot.transform.right = direction;

            SetColliders(currentBuilding, true);
            currentBuilding = null;
            structure = null;
            placingRoad = false;
        }

    }

    public void StartPlacingRoad()
    {
        placingRoad = true;
    }

    public void UpgradeHouse()
    {

    }

    public void SetColliders(GameObject go, bool value)
    {
        foreach (Collider collider in go.GetComponentsInChildren<Collider>())
        {
            collider.enabled = value;
        }
    }
    void LateUpdate()
    {

        if (structure != null)
        {
            truePos.x = Mathf.Floor(target.transform.position.x / gridSize) * gridSize;
            truePos.y = target.transform.position.y;//Mathf.Floor(target.transform.position.y / gridSize) * gridSize;
            truePos.z = Mathf.Floor(target.transform.position.z / gridSize) * gridSize;

            structure.transform.position = truePos;
        }


    }
}
